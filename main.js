// recuperation des donnees (activites)
var listeActivites = myJson.activites;

/* retourne la couleur HSL en fonction de la note */
function hslColor(note){
	var res = "hsl";
	if (note < 5){
		// hsl(0,100%, 50%) à hsl(0,100%,85%)
		res += "(0,100%,"+ ((note*7)+50).toString() + "%)";
	}else{
		// hsl(235,100%, 85%) à hsl(235,100%,50%)
		res += "(235,100%," + (85-((note-5)*7)).toString() + "%)";
	}
	return res;
}


/* charge le tableau des activites pour la classe */
function chargerTabActivites (){
	var tabActivites = document.getElementById("tabListeActivites");
	tabActivites.innerHTML = "";

	// création d'une ligne dans le tableau pour chaque activites
	for (var i = 0; i < listeActivites.length; i++) {
		var moyReuDur = getReussiteDureeMoyenne(listeActivites[i]);
		var elemTab = ""
		elemTab += "<tr><td><a href=\"#\"";
		elemTab += " onclick=\"afficherActiviteClasse("+ i.toString() +")\">"+ listeActivites[i].nom +"</a>";
		elemTab += "</td><td>"+ moyReuDur[0] +"</td><td>"+ moyReuDur[1] +"</td></tr>";
		tabActivites.innerHTML += elemTab;
	}
}

/* charge la liste des eleves pour la liste deroulante */
function chargerListeEleve(){
	var choixEleve = document.getElementById("choixEleve");
	choixEleve.innerHTML = "";

	for (var i = 0; i < listeActivites[0].resultats.length; i++){
		var nomPrenom = listeActivites[0].resultats[i].nom + " " + listeActivites[0].resultats[i].prenom;
		var elem = "";
		elem += "<option>" + nomPrenom + "</option>"
		choixEleve.innerHTML += elem;
	}
}


/* cache toutes les div de la zone principale (milieu) */
function cacherPrincipal(){
	var p = document.getElementById("zonePrincipale");
	var listPages = p.getElementsByTagName('div');
	for (var i = 0; i < listPages.length; i++){
		listPages[i].classList.add('hidden');
	}
}

/* cache toutes les div de la zone eleve */
function cacherZoneEleve(){
	var p = document.getElementById("zoneEleves");
	var listPages = p.getElementsByTagName('div');
	for (var i = 0; i < listPages.length; i++){
		listPages[i].classList.add('hidden');
	}
}

/* affiche la page d'accueil */
function afficherAccueil(){
	// cache toutes les pages du millieu
	cacherPrincipal();
	// cache toutes les pages de la zone eleve
	cacherZoneEleve();
	// affiche la page d'accueil
	document.getElementById("accueil").classList.remove('hidden');

}

/* retourne la reussite moyenne et la duree moyenne d'une activite act */
function getReussiteDureeMoyenne(act){
	var listeRes = act.resultats;
	var reussiteMoyenne = 0;
	var dureeMoyenne = 0;
	for (var i = 0; i <= listeRes.length -1; i++){
		reussiteMoyenne += Number(listeRes[i].reussite);
	}
	return [reussiteMoyenne/listeRes.length, 0];
}

/* affiche la liste des activités pour la classe (zone principale) */
function afficherListeActivitesClasse() {
	// cache toutes les pages du millieu
	cacherPrincipal();
	cacherZoneEleve();
	
	// affiche la liste des activites classe
	document.getElementById("listeActivitesClasse").classList.remove('hidden');

	// affiche la zone eleve
	document.getElementById("listeElevesClasse").classList.remove('hidden');

	// affiche la liste des eleves et les donnees moyennes
	chargerTabElevesMoyenne();

}

/* affiche l'activité nb pour la classe (zone principale) */
function afficherActiviteClasse(nb){
	// tout cacher
	cacherPrincipal();
	cacherZoneEleve();

	// affiche l'activite nb
	var div = document.getElementById("activiteClasse");
	div.classList.remove('hidden');

	// recupere les donnees de l'activite nb
	var act = listeActivites[nb];

	document.getElementById("nomActiviteClasse").innerHTML = act.nom;

	var moyReuDur = getReussiteDureeMoyenne(act);
	document.getElementById("reussiteActiviteClasse").innerHTML = moyReuDur[0];
	document.getElementById("dureeActiviteClasse").innerHTML = moyReuDur[1];
	


	// affiche et charge le diagramme associé a cette activite
	document.getElementById("zoneChart").classList.remove("hidden");
	document.getElementById("chartActivite").classList.remove("hidden");
	chargerDiagrammeActivite(nb);

	// affiche les checkbox
	document.getElementById("ensCBox").classList.remove("hidden");


	// affiche la liste des eleves avec les données de l'activite nb
	document.getElementById("listeElevesClasse").classList.remove("hidden");
	chargerTabElevesAct(nb);

}

/* charger le diagramme de l'activité nb */
function chargerDiagrammeActivite(nb){
	var listeRes = listeActivites[nb].resultats;

	var res = [0,0,0,0];
	for (var i = 0; i < listeRes.length; i++){
		if (Number(listeRes[i].reussite) < 2.5){
			res[0]++;
		}else if(Number(listeRes[i].reussite) < 5){
			res[1]++;
		}else if(Number(listeRes[i].reussite) < 7.5){
			res[2]++;
		}else{
			res[3]++;
		}
	}

	var data = {
		labels: res,
		series: [res]
	}
	var options = {
		axisX: {
			offset: 10,
			showGrid:false
		}
	}
	new Chartist.Bar('#chartActivite', data, options)
	.on('draw', function(data){
		if (data.type == 'bar'){
			data.element.attr({
				style: "stroke-width: 40"
				//style: 'stroke-width:' + ((100/res.length)-1) +'%'
			});
		}
	});
}


/* charger la liste des eleves avec les notes de l'activité nb en fonction des checkbox cochés (zone eleve) */
function chargerTabElevesAct(nb){
	// changement du nom des colonnes
	document.getElementById("tabReussite").innerHTML = "Reussite (/10)";
	document.getElementById("tabTemps").innerHTML = "Temps";


	var boxes = document.getElementsByName("cbox");
	var listeRes = listeActivites[nb].resultats;
	var tabListEleves = document.getElementById("tabListEleves");
	tabListEleves.innerHTML = "";
	for (var i = 0; i < listeRes.length; i++){
		if ((Number(listeRes[i].reussite) < 2.5 && boxes[0].checked == true)
			|| (2.5 <= Number(listeRes[i].reussite) && Number(listeRes[i].reussite) < 5 && boxes[1].checked == true)
			|| (5 <= Number(listeRes[i].reussite) && Number(listeRes[i].reussite) < 7.5 && boxes[2].checked == true)
			|| (7.5 <= Number(listeRes[i].reussite) && boxes[3].checked == true)){
			var elem = "";
			elem += "<tr><td><a href=\"#\" onclick=\"afficherActiviteEleve2("+ i.toString() + "," + nb +")\">" + listeRes[i].nom + " " + listeRes[i].prenom + "</td>";
			elem += "<td class=\"noteColor\" style=\"background-color:" + hslColor(Number(listeRes[i].reussite)) +"\">" + listeRes[i].reussite + "</td>";
			elem += "<td>" + listeRes[i].duree + "</td></tr>";
			tabListEleves.innerHTML += elem;
		}
	}
}


// retourne l'indice de l'activite nomAct
function getIndiceAct (nomAct){
	for (var i = 0; i < listeActivites.length; i++){
		if (listeActivites[i].nom == nomAct){
			return i;
		}
	}
	return -1;
}

/* appelle la fonction chargerTabElevesAct lorqu'un checkbox est coché/déchoché */
function actualiserTabElevesAct(){
	var nomAct = document.getElementById("nomActiviteClasse").innerHTML;
	nb = getIndiceAct(nomAct);
	if (nb >= 0){
		chargerTabElevesAct(nb);
	}
}


// retourne la moyenne et le temps cumulé de l'eleve numero numEleve
function getMoyenneTempsCumEleve(numEleve){
	var note = 0;
	var tempsCum = 0;
	for (var i = 0; i < listeActivites.length; i++){
		note += listeActivites[i].resultats[numEleve]
	}
}

/* charge le tableau contenant la liste des eleves et leur moyenne de toutes les activites (zone eleve) */
function chargerTabElevesMoyenne(){
	// changement du nom des colonnes
	document.getElementById("tabReussite").innerHTML = "Reussite moyenne";
	document.getElementById("tabTemps").innerHTML = "Temps cumulé";

	var listAct = listeActivites;
	var tabListEleves = document.getElementById("tabListEleves");
	tabListEleves.innerHTML = "";
	for (var i = 0; i < listeActivites[0].resultats.length; i++){
		var nomPrenom = listeActivites[0].resultats[i].nom + " " + listeActivites[0].resultats[i].prenom;
		var note_i = 0;
		var tempsCum_i = 0;
		elem = "";
		for (var j = 0; j < listeActivites.length; j++){
			note_i += Number(listeActivites[j].resultats[i].reussite);

		}
		
		var noteMoyi = note_i/j;
		elem += "<tr><td><a href=\"#\" onclick=\"afficherListeActivitesEleve2("+ i.toString() +")\">" + nomPrenom + "</td><td class=\"noteColor\" style=\"background-color:" + hslColor(noteMoyi) +"\">" + noteMoyi + "</td><td>" + tempsCum_i + "</td></tr>";
		tabListEleves.innerHTML += elem;
	}
}


/* affiche la liste des activites pour un eleve (zone principale) */
function afficherListeActivitesEleve(){
	// recupere d'abord l'eleve en question (de la liste deroulante)
	var numEleve = document.getElementById("choixEleve").selectedIndex;

	// tout cacher
	cacherPrincipal();
	cacherZoneEleve();

	// affiche la liste des activites pour un eleve
	document.getElementById("listeActivitesEleve").classList.remove('hidden');

	// affiche la zone de l'eleve
	document.getElementById("pageEleve").classList.remove('hidden');

	chargerTabActivitesEleve(numEleve);

}


/* charge le tableau contenant la liste des activites pour un eleve */
function chargerTabActivitesEleve(){
	// recupere d'abord l'eleve en question (de la liste deroulante)
	var numEleve = document.getElementById("choixEleve").selectedIndex;

	var tabActivitesEleve = document.getElementById("tabListeActivitesEleve");
	tabActivitesEleve.innerHTML = "";

	// création d'une ligne dans le tableau pour chaque activites
	for (var i=0; i < listeActivites.length; i++){
		var elem = "";
		elem += "<tr><td><a href=\"#\" onclick=\"afficherActiviteEleve("+ i.toString() + "," + numEleve.toString() + ")\">"+ listeActivites[i].nom + "</a></td>";
		elem += "<td>" + listeActivites[i].resultats[numEleve].reussite + "</td>";
		elem += "<td>" + listeActivites[i].resultats[numEleve].duree + "</td></tr>";
		tabActivitesEleve.innerHTML += elem;
	}
}

/* affiche la page de l'activite nb pour un eleve (zone principale) */
function afficherActiviteEleve(nb){
	// recupere d'abord l'eleve en question (de la liste deroulante)
	var numEleve = document.getElementById("choixEleve").selectedIndex;

	// tout cacher
	cacherPrincipal();
	cacherZoneEleve();

	// affiche la zone de l'eleve
	document.getElementById("pageEleve").classList.remove('hidden');
	
	// affiche la page activite eleve
	var div = document.getElementById("activiteEleve");
	div.classList.remove('hidden');

	// recupere les donnes de l'activite nb pour l'eleve numEleve
	var act = listeActivites[nb];

	document.getElementById("nomActiviteEleve").innerHTML = act.nom;
	document.getElementById("reussiteActiviteEleve").innerHTML = act.resultats[numEleve].reussite;
	document.getElementById("dureeActiviteEleve").innerHTML = act.resultats[numEleve].duree;

	//
	var btn = div.getElementsByTagName("btn");
	btn = "<button id=\"btnRetour\" class=\"btn\" onclick=\"afficherListeActivitesEleve()\">Retour</button>";
}

/* actualise la page principale lorsqu'on selectionne un autre eleve dans la liste deroulante */
function actualiserPageEleve(){
	if(document.getElementById("activiteEleve").classList.contains("hidden")){
		afficherListeActivitesEleve();
	}else{
		var nb = getIndiceAct(document.getElementById("nomActiviteEleve").innerHTML);
		afficherActiviteEleve(nb);
	}
}


// ces 2 fonctions permettent d'aller aux pages eleves à partir des tableaux de la zone eleve
/* affiche la page de l'activite nb pour un eleve (zone principale) en modifiant l'eleve dans la liste deroulante par l'eleve numEleve */
function afficherActiviteEleve2(numEleve, nb){
	document.getElementById("choixEleve").selectedIndex = numEleve;
	afficherActiviteEleve(nb);
}

/* affiche la liste des activites pour un eleve (zone principale) en modifiant l'eleve dans la liste deroulante par l'eleve numEleve */
function afficherListeActivitesEleve2(numEleve){
	document.getElementById("choixEleve").selectedIndex = numEleve;
	afficherListeActivitesEleve();
}